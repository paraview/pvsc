# This is a helper function to compare version strings.
# Thanks stackoverflow! http://stackoverflow.com/a/14652197

# Breaks the version string into a list of tokens
proc tokenize {v} {
	set v [string map { " " "" } $v]
	set result {}
	foreach token [split $v ".-"] {
		set tokens_scanned [scan $token "%d%s" number alpha]
		if {$tokens_scanned == 0} {lappend result $token}         ;# is alpha, e.g. beta
		if {$tokens_scanned == 1} {lappend result $number}        ;# is number, e.g. 5
		if {$tokens_scanned == 2} {lappend result $number $alpha} ;# is both, e.g. 5beta
	}
	return $result
}

# Examples of versions this function handles:
# 1, 1a, 1.5, 3.2b, 3.2.1, 3.2.1-beta1
proc compare_version {v1 v2} {
	# Sanitize the data
	set v1 [tokenize $v1]
	set v2 [tokenize $v2]

	foreach token1 $v1 token2 $v2 {
		if {$token1 < $token2} { return -1 }
		if {$token1 > $token2} { return 1 }
	}
	return 0
}
