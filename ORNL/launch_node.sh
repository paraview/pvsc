#!/usr/bin/env bash

echo "---------- BEGIN PVSERVER LAUNCHER ----------"
date +"%F %T"
echo "---------------------------------------------"

source "${BASE_DIR}/../Common/pvserver_jobsub.functions"
source "${CONF_PATH}"

if [ -n "${PARAVIEW_MODULE_DIRS}" ]; then
    for path in ${PARAVIEW_MODULE_DIRS}; do
        module use ${path}
    done
fi

if [ "$HEADLESS_API" == "rocm" ]; then
    if ! module load "paraview/${PV_VERSION_FULL}-${HEADLESS_API}"; then
        echo "Module does not exist: paraview/${PV_VERSION_FULL}-${HEADLESS_API}"
        exit 1
    fi
else
    if ! module load "paraview/${PV_VERSION_FULL}"; then
        echo "Module does not exist: paraview/${PV_VERSION_FULL}"
        exit 1
    fi
fi
# good for debugging
module list

if ! setup_tunnel_socat ${SUB_IF} ${PV_SERVER_HOST} ${PV_SERVER_PORT} .paraview/pvserver-socat-launch.log
then
  echo "Failed to setup tunnel on submission host"
  exit 1
fi
export PV_SERVER_HOST=${IP}
echo "Setting ${THREADS_PER_CORE} threads per process"

if [ "${DRIVER}" = "swr" ]
then
  export GALLIUM_DRIVER=swr
  export KNOB_MAX_WORKER_THREADS=${THREADS_PER_CORE}
else
  export GALLIUM_DRIVER=llvmpipe
  export LP_NUM_THREADS=${THREADS_PER_CORE}
fi
export OSPRAY_THREADS=${THREADS_PER_CORE}
export OMP_NUM_THREADS=${THREADS_PER_CORE}
export KMP_NUM_THREADS=${THREADS_PER_CORE}

exec_pvserver
